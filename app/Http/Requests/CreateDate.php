<?php

namespace App\Http\Requests;

use Illuminate\Contracts\Validation\Validator;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Http\JsonResponse;
use Illuminate\Support\Facades\Route;
use Illuminate\Validation\ValidationException;

class CreateDate extends FormRequest
{
    public static $rules = [];
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $rules = Self::$rules;
        $data = $this::all();
        $data = $data['data'];

        switch (Route::currentRouteName()) {
            case 'delivery-date.store':
            {
                $rules['data.title'] = 'required';
                $rules['data.delivery_text'] = 'required';

                if( $data['applies_to'] == 'specific_product'){
                    $rules['data.specific_product'] = 'required|array|min:1';
                }elseif ( $data['applies_to'] == 'specific_collection' ) $rules['data.specific_collection'] = 'required|array|min:1';

                foreach( $data['countries'] as $key=>$val){
                    str_replace(' ', '', $key);
                    $rules['data.countries.'.$key.'.0'] = 'numeric|min:1';
                    $rules['data.countries.'.$key.'.1'] = 'numeric|min:1';
                }
                return $rules;
            }

            default:
                break;
        }
    }

    /**
     * Get custom messages for validator errors.
     *
     * @return array
     */
    public function messages()
    {
        $rules = [];
        $data = $this::all();
        $data = $data['data'];
        $rules['data.title.*'] = 'required';

        if( $data['applies_to'] == 'specific_product'){
            $rules['data.specific_product.*'] = 'Select atleast one product';
        }elseif ( $data['applies_to'] == 'specific_collection' ){
            $rules['data.specific_collection.*'] = 'Select atleast one collection';
        }
//        foreach( $data['countries'] as $key=>$val){
//            $rules['data.countries.' . $key . '.0'] = 'Min value shou';
//            $rules['data.countries.' . $key . '.1'] = 'required|numeric|min:1';
//        }

        return $rules;
    }

    protected function failedValidation(Validator $validator)
    {
        if ($this->ajax() || $this->wantsJson()) {
            $response = new JsonResponse($validator->errors(), 422);
            throw new ValidationException($validator, $response);
        }

        throw (new ValidationException($validator))
            ->errorBag($this->errorBag);
    }
}
