<?php

namespace App\Http\Controllers\DeliveryDate;

use App\Http\Controllers\Controller;
use App\Http\Requests\CreateDate;
use Illuminate\Http\Request;

class DeliveryDateController extends Controller
{
    public function index( Request $request ){
        try{
            $arr = ['rest_of_world' =>  [3, 2]];
            dd(json_encode($arr));
            return response()->json(['data' => $data], 200);
        }catch ( \Exception $e ){
            return response()->json(['data' => $e->getMessage()], 422);
        }
    }

    public function store( CreateDate $request){
        try{
            dd($request);
        }catch ( \Exception $e ){
            return response()->json(['data' => $e->getMessage()], 422);
        }
    }

    public function getCountry(){
        try{
            $data['countries'] = array('Rest of World' => 'Rest of World') + countryH();
            return response()->json(['data' => $data], 200);
        }catch ( \Exception $e ){
            return response()->json(['data' => $e->getMessage()], 422);
        }
    }
}
