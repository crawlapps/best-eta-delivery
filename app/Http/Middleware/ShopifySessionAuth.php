<?php

namespace App\Http\Middleware;

use App\Models\User;
use Closure;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Session;
use Osiset\ShopifyApp\Contracts\Objects\Values\ShopDomain as ShopDomainValue;
use Osiset\ShopifyApp\Services\ShopSession;

class ShopifySessionAuth
{
    private $shopDomain = null;

    /**
     * The shop session helper.
     *
     * @var ShopSession
     */
    private $shopSession;

    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if($this->tryLoginWithShopifyJwtToken($request))
        {
            $domain = $this->shopDomain->toNative();
            $user = User::where('name', $domain)->first();
            if( $user ){
                Auth::login($user);
                return $next($request);
            }
            return Redirect::route('login');
        }
    }

    private function tryLoginWithShopifyJwtToken(Request $request): bool
    {
        if( $request->bearerToken())
        {
            $exploded = explode('.', $request->bearerToken(), 3);

            if (3 == \count($exploded)) {
                list($header, $payload, $signature) = $exploded;

                $payloadArray = json_decode(base64_decode($payload), true);

                if ($payloadArray['exp'] > now()->utc()->timestamp
                    && $payloadArray['nbf'] <= now()->utc()->timestamp
                    && \Illuminate\Support\Str::startsWith($payloadArray['iss'], $payloadArray['dest'])
                ) {
                    $this->shopDomain = \Osiset\ShopifyApp\Objects\Values\ShopDomain::fromNative($payloadArray['dest']);
                    if (! $this->shopDomain->isNull()) {
                        $shop = resolve(\Osiset\ShopifyApp\Contracts\Queries\Shop::class)->getByDomain($this->shopDomain);

                        if ($shop) {
                            // Generate api helper from shop
                            $apiHelper = $shop->apiHelper();

                            $apiSecret = $apiHelper->getApi()->getOptions()->getApiSecret();

                            $hmacLocal = rtrim(strtr(base64_encode(hash_hmac('sha256', $header.'.'.$payload, $apiSecret, true)), '+/', '-_'), '=');

                            if (hash_equals($hmacLocal, $signature)) {
//                                $checks[] = 'loginShop';
//                                // Verify the Shopify session token and verify the shop data
////                                array_push($checks, 'verifyShopifySessionToken', 'verifyShop');
//                                array_push($checks,  'verifyShop');
//
//                                // Loop all checks needing to be done, if we get a false, handle it
//                                foreach ($checks as $check) {
//                                    $result = call_user_func([$this, $check], $request, $this->shopDomain);
//                                    if ($result === false) {
//                                        return $this->handleBadVerification($request, $this->shopDomain);
//                                    }
//                                }
                                return true;
//                                return $this->loginShop($request, $this->shopDomain);
                            }
                        }
                    }
                }
            }
        }
        if ($this->shopDomain->isNull()) {
            // We have no idea of knowing who this is, this should not happen
            return Redirect::route('login');
        }else{
            return Redirect::route(
                'authenticate.oauth',
                ['shop' => $this->shopDomain->toNative()]
            );
        }

    }

    /**
     * Login and verify the shop and it's data.
     *
     * @param Request         $request The request object.
     * @param ShopDomainValue $domain  The shop domain.
     *
     * @return bool
     */
    private function loginShop(Request $request, ShopDomainValue $domain): bool
    {
        // Log the shop in
        $status = $this->shopSession->make($domain);
        if (!$status || !$this->shopSession->isValid()) {
            // Somethings not right... missing token?
            return false;
        }

        return true;
    }

    /**
     * @param  Request  $request
     * @param  ShopDomainValue  $domain
     * @return bool
     */
    private function verifyShop(Request $request, ShopDomainValue $domain): bool
    {
        // Grab the domain
        if (!$domain->isNull() && !$this->shopSession->isValidCompare($domain)) {
            // Somethings not right with the validation
            return false;
        }

        return true;
    }
    private function handleBadVerification(Request $request, ShopDomainValue $domain)
    {
        if ($domain->isNull()) {
            // We have no idea of knowing who this is, this should not happen
            return Redirect::route('login');
        }

        // Set the return-to path so we can redirect after successful authentication
        Session::put('return_to', $request->fullUrl());

        // Kill off anything to do with the session
//        $this->shopSession->forget();

        // Mis-match of shops
        return Redirect::route(
            'authenticate.oauth',
            ['shop' => $domain->toNative()]
        );
    }
}
