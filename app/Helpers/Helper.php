<?php
use DougSisk\CountryState\CountryState;

if (!function_exists('countryH')) {
    /**
    * @return array
    */
    function countryH()
    {
        $newsletter = app(CountryState::class);
        $countries = $newsletter->getCountries();
        asort($countries);
        return $countries;
    }
}
