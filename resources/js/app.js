import Vue from 'vue'
require('./bootstrap');
window.Vue = require('vue');

import App from './components/layouts';
import router from './routes';

const app = new Vue({
    el: '#app',
    router,
    render: h => h(App),
});
