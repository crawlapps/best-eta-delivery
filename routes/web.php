<?php

use App\Http\Controllers\Auth\AuthController;
use App\Http\Controllers\DeliveryDate\DeliveryDateController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

//Route::get('/', function () {
//    return view('welcome');
//});
Route::get(
    '/login', [AuthController::class,'index']
)->name('login');

Route::get('/', function () {
    return view('layouts.app');
})->middleware(['auth.shopify'])->name('home');

Route::group(['middleware' => ['auth.shopifysession']], function (){
    Route::resource('delivery-date', DeliveryDateController::class);
    Route::get('get-country', [DeliveryDateController::class, 'getCountry'])->name('get-country');
});
//Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home');
